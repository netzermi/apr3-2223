package at.hakimst.apr.strategy;

import java.util.List;

public class CSVStrategy implements ExportStrategy{

    @Override
    public String export(List<Kunde> kundenListe) {
        StringBuilder csv = new StringBuilder();
        csv.append("id;vorname;nachname\n");
        for(Kunde k : kundenListe){
            csv.append(k.getId()+ ";" + k.getVorname() + ";" + k.getNachname() + "\n");
        }
        return csv.toString();
    }
}
