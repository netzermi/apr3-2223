package at.hakimst.apr.strategy;

import java.util.List;

public interface ExportStrategy {

    public String export(List<Kunde> kundenListe);
}
