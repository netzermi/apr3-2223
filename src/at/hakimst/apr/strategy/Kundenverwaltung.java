package at.hakimst.apr.strategy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Kundenverwaltung {
    private List<Kunde> kundenListe;
    private ExportStrategy exportStrategy;

    public Kundenverwaltung(){
        this.kundenListe = new ArrayList<>();
    }

    public void kundeHinzufgen(Kunde kunde){
        kundenListe.add(kunde);
    }

    public void setExportStrategy(ExportStrategy exportStrategy){
        this.exportStrategy = exportStrategy;
    }

    public void exportData(String filename){
        if(exportStrategy == null){
            throw new RuntimeException("Error: Export strategy not set");
        }
        String s = exportStrategy.export(kundenListe);
        try{
            FileWriter fileWriter = new FileWriter(filename);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(s);
            bufferedWriter.close();
        } catch(IOException e){
            e.printStackTrace();
        }

    }




}
