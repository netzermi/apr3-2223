package at.hakimst.apr.strategy;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        Kunde k1 = new Kunde(UUID.randomUUID(),"Max", "Mustermann");
        Kunde k2 = new Kunde(UUID.randomUUID(), "Susi", "Sorglos");
        Kundenverwaltung kv = new Kundenverwaltung();
        kv.kundeHinzufgen(k1);
        kv.kundeHinzufgen(k2);
        kv.setExportStrategy(new CSVStrategy());
        kv.exportData("export.csv");


    }
}
