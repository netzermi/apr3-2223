package at.hakimst.apr.testvb;
/* Beispiel einer hohen Kopplung: Das Fahrzeug kennt auch die Garage -> Nicht notwendig */
public class Fahrzeug implements IDriveable{
    private String name;


    /* Beispiel einer hohen Kopplung: Das Fahrzeug kennt auch die Garage -> Nicht notwendig */
    public Fahrzeug(String name){
        this.name = name;
    }

    public void fahren(){
        System.out.println("Das Fahrzeug fährt los....");
    }

    @Override
    public void drive() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void accelerate() {

    }

    @Override
    public void honk() {

    }

    @Override
    public void indicate() {

    }
}
