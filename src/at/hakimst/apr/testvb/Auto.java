package at.hakimst.apr.testvb;

public class Auto extends Fahrzeug{

    public Auto(String name){
        super(name);
    }

    public void fahren(){
        System.out.println("Das Auto fährt los...");
    }
}
