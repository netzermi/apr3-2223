package at.hakimst.apr.testvb;
/* Beispiel einer hohen Kopplung: Das Fahrzeug kennt auch die Garage -> Nicht notwendig */
public class FahrzeugSchlechteKopplung {
    private String name;
    private Garage garage;


    /* Beispiel einer hohen Kopplung: Das Fahrzeug kennt auch die Garage -> Nicht notwendig */
    public FahrzeugSchlechteKopplung(String name, Garage garage){
        this.name = name;
        this.garage = garage;
    }
}
