package at.hakimst.apr.testvb;

public interface IDriveable {

    public void drive();
    public void stop();
    public void accelerate();

    //Folgende Methoden verletzen das Interface Segregation Prinzip
    //Die folgenden Methoden sollten besser in ein eigenes Interface
    //verschoben werden (z.B. ITrafficRegulators).
    public void honk();
    public void indicate();
}
