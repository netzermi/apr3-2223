package at.hakimst.apr.testvb;

public class StatUtil {

    public static void main(String[] args) {
        double[] ar = {3.4, 5.6, 5};
        System.out.println(mean(ar));
    }

    /* Beispiel für eine Methode mit einer geringen Kohäsion = schlecht */
    public static double mean(double[] values){
        double mean = 0d;
        for(double d : values){
            mean += d;
        }
        mean /= values.length;
        //mean = mean/values.length;
        System.out.println("The mean is: " + mean);
        return mean;
    }
}
