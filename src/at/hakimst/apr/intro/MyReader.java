package at.hakimst.apr.intro;

import jdk.jshell.spi.ExecutionControlProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {
    private String filename;

    public MyReader(String filename){
        this.filename = filename;

    }

    public String getData()  {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(this.filename));
            return reader.readLine();
        } catch (Exception e) {
            System.out.println("Fehler beim Lesen");
        }
        return null;
    }


}
