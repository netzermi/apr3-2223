package at.hakimst.apr.intro;

import java.io.*;

public class App2 {

    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {
        Kunde k1 = new Kunde(7, "Müller");
        Kunde k2 = (Kunde) k1.clone();
        k1.setNachname("Obermüller");
        System.out.println(k1.getNachname());
        System.out.println(k2.getNachname());

        FileOutputStream fos = new FileOutputStream("output");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(k1);
        fos.close();
        oos.close();

        FileInputStream fis = new FileInputStream("output");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Kunde k3 = (Kunde) ois.readObject();
        System.out.println(k3.getNachname());



    }
}
