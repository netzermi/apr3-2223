package at.hakimst.apr.intro;

public class Auto extends Fahrzeug{
    public Auto(){
        super();
    }
    public Auto(int gewicht){
        super(gewicht);
    }

    @Override
    public void fahren() {
        System.out.println("Das Auto fährt los....");
    }
}
