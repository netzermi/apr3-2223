package at.hakimst.apr.intro;

import java.io.Serializable;

public class Kunde implements Cloneable, Serializable {
    public static long counter = 0;

    private long kundennummer;
    private String nachname;

    public Kunde(long kundennummer, String nachname){
        this.kundennummer = kundennummer;
        this.nachname = nachname;
        counter++;
    }

    public String getNachname(){
        return nachname;
    }

    public long getKundennummer() {
        return kundennummer;
    }

    public static long getKey(){
        return counter;
    }

    public void setKundennummer(long kundennummer) {
        this.kundennummer = kundennummer;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
