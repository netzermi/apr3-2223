package at.hakimst.apr.intro;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Fahrzeug f1 = null;
        System.out.println("Was möchtest du? Auto (1) oder Motorrad (2)");
        int eingabe = scanner.nextInt();
        if(eingabe == 1){
            f1 = new Auto();
        } else if (eingabe == 2){
            f1 = new Motorrad();
        } else {
            System.out.println("1 oder 2 zu wählen ist wirklich schwierig!");
        }
        f1.fahren();


    }
}
