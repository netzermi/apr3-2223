package at.hakimst.apr.intro;

public class StringExamples {

    public static void main(String[] args) {
        String s = "";
        long start = System.currentTimeMillis();
        for(int i = 10000; i > 0; i--){
            s += "o";
        }
        long end = System.currentTimeMillis();
        long dur = end-start;
        System.out.println(dur);
        System.out.println(s);

    }
}
