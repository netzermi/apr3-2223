package at.hakimst.apr.intro;

import java.util.Scanner;

public class MainLoop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wie hoch soll der Baum werden?");
        int höhe = scanner.nextInt();

        for (int i = 0; i < höhe; i++) {
            for (int ziffer = höhe; ziffer > i+1; ziffer--) {
                System.out.print(" ");
            }
            System.out.print("*");
            for (int ziffer = 0; ziffer < i; ziffer++) {
                System.out.print("**");
            }
            System.out.println(" ");
        }
    }
}
