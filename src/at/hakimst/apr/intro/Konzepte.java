package at.hakimst.apr.intro;

public class Konzepte {

    public static void main(String[] args) {
        Fahrzeug fahrzeug = null;
        if(Math.random() < 0.5){
            fahrzeug = new Motorrad();
        } else {
            fahrzeug = new Auto();
        }
        fahrzeug.fahren();
    }
}
