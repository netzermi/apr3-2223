package at.hakimst.apr.intro;

public class Motorrad extends Fahrzeug{

    public Motorrad(){
        super();
    }

    public Motorrad(int gewicht){
        super(gewicht);
    }

    @Override
    public void fahren() {
        System.out.println("Motorrad fährt los....Brumm...");
    }
}
