package at.hakimst.apr.intro;

import java.util.ArrayList;
import java.util.List;

public class App4 {

    public static void main(String[] args) {
        List<Fahrzeug> fahrzeuge = new ArrayList<>();
        fahrzeuge.add(new Auto(1500));
        fahrzeuge.add(new Motorrad(120));
        fahrzeuge.add(new Auto(900));
        int gesamtgewicht = 0;
        for(Fahrzeug f : fahrzeuge){
            gesamtgewicht += f.getGewicht();
           // gesamtgewicht = gesamtgewicht + f.getGewicht();
        }
        System.out.println(gesamtgewicht);

    }
}
