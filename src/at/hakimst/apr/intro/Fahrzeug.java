package at.hakimst.apr.intro;

public abstract class Fahrzeug {
    private int gewicht;
    private String hersteller;
    private String modell;


   public Fahrzeug(){
   }

   public Fahrzeug(int gewicht){
       this.gewicht = gewicht;
   }
    public Fahrzeug(int gewicht, String hersteller, String modell){
        this.gewicht = gewicht;
        this.hersteller = hersteller;
        this.modell = modell;
    }

    public void fahren(){
        fahren(10);
    }

    public void fahren(int distanz){
        System.out.println("Fahrzeug wird um " + distanz + " bewegt!");
    }


    public int getGewicht() {
        return gewicht;
    }
}
